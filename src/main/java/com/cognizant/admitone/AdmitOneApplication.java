package com.cognizant.admitone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdmitOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdmitOneApplication.class, args);
	}

}
